#include <iostream>
#include <typeinfo>
#include <string>

void output(int);
void output(char);
void output(long);
void output(char[], int = 1, int = 0);
void output(float);

int main()
{
    char myname[] = {"ernest"};
    output(3.14f);
    output(myname, 2);
    // output(myname);
}

void output(int a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(char a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(long a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(float a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(char a[], int b, int c)
{
    if (c)
        for (int i = b - 1; i < c; ++i)
            std::cout << a[i];    
    else
        for (int i = b - 1; a[i]; ++i)
            std::cout << a[i]; 

    std::cout << " - is of type: " << typeid(a).name() << std::endl;
}