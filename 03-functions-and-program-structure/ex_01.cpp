#include <iostream>

int input(int a, int b);

int main()
{
    int a;
    int b;
    std::cout << "Enter two integers: ";
    std::cin >> a >> b;
    std::cout << input(a, b) << std::endl;
}

int input (int a, int b) 
{
    return a + b;
}