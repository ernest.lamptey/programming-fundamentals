#include <iostream>
#include <typeinfo>
#include <string>

void output(int);
void output(char);
void output(long);
void output(std::string);
void output(float);

int main()
{
    std::string myname {"ernest"};
    output(3.14f);
    output("hi there");
    output(myname);
}

void output(int a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(char a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(long a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}

void output(float a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}


void output(std::string a)
{
    std::cout << a << " - is of type: " << typeid(a).name() << std::endl;
}