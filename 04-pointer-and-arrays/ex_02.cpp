#include <iostream>

namespace my {

    int strlen(const char *s)
    {
        int count {0};
        for (int i = 0; s[i] != '\0'; ++i)
            ++count;

        return count;
    }

    int strcmp (const char *l, const char *r)
    {
        int l_value;
        int r_value;

        for (int i = 0; i < strlen(l); ++i)
            l_value += int (l[i]);

        for (int j = 0; j < strlen(r); ++j)
            r_value += int (r[j]);

        // std::cout << l_value << std::endl;
        // std::cout << r_value << std::endl;

        return l_value - r_value;
    }

}

int main()
{
    std::cout << my::strcmp("ABC", "AB") << std::endl;
}