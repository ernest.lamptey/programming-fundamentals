#include <iostream>
#include <string>
#include <typeinfo>

int main()
{
    std::string word;
    std::cin >> word;

    const char *ptr {word.c_str()};

    for (int i = 0; ptr[i] != '\0'; ++i)
        std::cout << ptr[i] << std::endl;
}