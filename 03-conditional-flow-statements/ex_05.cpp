#include <iostream>
#include <string>
#include <vector>

int main()
{
    char input[255];
    std::cin.get(input, 255);

    std::vector<std::string> word_list;

    std::string temp_word;
    
    for (auto i : input)
    {
        if (i != ' ' && i != '\0') 
            temp_word.push_back(i);
        else
        {
            word_list.push_back(temp_word);
            temp_word.clear();
        }

    }

    for (auto word : word_list)
    {
        if (word.length() > temp_word.length())
        {
            temp_word = word;
        }
    }

    std::cout << temp_word << std::endl;
}   