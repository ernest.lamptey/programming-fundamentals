#include <iostream>
#include <string>

int main()
{
    std::cout << "Please input two operands: ";
    int value_1, value_2;
    std::cin >> value_1 >> value_2;
    // std::cout << value_1 << ", " << value_2 << std::endl;

    std::cout << "Please input an operator: ";
    char operator_symbol;
    std::cin >> operator_symbol;

    switch (operator_symbol)
    {
        case '*':
            std::cout << "Multiplying " << value_1 << " and " << value_2 << " = " << (value_1 * value_2) << std::endl; 
            break;

        case '+':
            std::cout << "Adding " << value_1 << " and " << value_2 << " = " << (value_1 + value_2) << std::endl; 
            break;
        
        case '-':
            std::cout << "Subtracting " << value_1 << " and " << value_2 << " = " << (value_1 - value_2) << std::endl; 
            break;

        case '/':
            std::cout << "Dividing " << value_1 << " and " << value_2 << " = " << (value_1 / value_2) << std::endl; 
            break;

        default:
            break;
    }
}