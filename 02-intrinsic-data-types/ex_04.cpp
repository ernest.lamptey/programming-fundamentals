#include <iostream>
#include <string>
#include <typeinfo>

int main()
{
    typedef std::string array_of_10[10];
    array_of_10 new_arr {"james", "roney", "jack"};
    
    std::string line;

    // while (std::getline(std::cin, line) && !line.empty())
    //     new_arr->append(line);
    for (auto i : new_arr)
        std::cout << i << std::endl;

    std::cout << "type of: " << new_arr << " is " << typeid(new_arr).name() << std::endl;
}