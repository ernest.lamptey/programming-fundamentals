#include <iostream>

int main () {
    int integer;
    int *integer_pointer {&integer};
    int &integer_reference {integer};
    const int integer_constant {integer};
    float a {0xf3f2};
    float b {0437};
    float c {'a'};

    integer = 5;
    ++*integer_pointer;
    ++integer_reference;
   // ++integer_constant;

    std::cout << a << " , " << b << " , " << c << std::endl; 
    std::cout << integer << std::endl;

}
