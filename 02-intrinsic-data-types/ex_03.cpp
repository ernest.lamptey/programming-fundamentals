#include <iostream>
#include <cstring>

int main() {
    char word[255];
    
    std::cout << "Enter a sentence separated by hyphens" << std::endl;
    std::cin >> word;

    // char *ptr_1 = &word[0];
    // std::cout << *ptr_1 << std::endl;
    // // char *ptr_2 = &word[4];

    int start_num {0};

    for (int i = 0; i <= strlen(word); ++i)
    {
        if(word[i] == '-' || word[i] == '\0')
        {
            std::cout << "[" << (i - start_num) << "] ";

            for (int j = start_num; j < i; ++j)
                std::cout << word[j];

            std::cout << "" << std::endl;

            if(word[i] != '\0')
                start_num = i + 1;
        }
    }
}