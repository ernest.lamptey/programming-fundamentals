#include <iostream>

int main() {
	int number;

	std::cout << "Please enter a number: ";
	std::cin >> number;

	if (number % 2 == 0) 
	{
		for (int iter = 1; iter <= 20; ++iter) {
			std::cout << iter << " x " << number << " = " << iter * number << std::endl;
		}
	}
       	else 
	{
		int counter = 0;
		int dividend = 1;

		while (counter < 30) {
			if (dividend % number != 0) {
				std::cout << dividend << std::endl;
				counter++;
			}
			dividend++;	
		}
	}
}
