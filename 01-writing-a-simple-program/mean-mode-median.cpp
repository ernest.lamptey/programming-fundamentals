#include <iostream>

int main() {
	std::cout << "Enter 5 integers. Hit return after each integer" << std::endl;

	int int1, int2, int3, int4, int5;
	float mean;

	std::cin >> int1 >> int2 >> int2 >> int4 >> int5;
	mean = (float)(int1 + int2 + int3 + int4 + int5) / 5;

	std::cout << "The mean is: " << mean << std::endl;
	std::cout << "Thank you for entering the values" << std::endl;
}
